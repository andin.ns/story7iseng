from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('/')
        self.assertFalse(response.status_code == 404)
