from django.apps import AppConfig


class Story7AppConfig(AppConfig):
    name = 'story7app'
